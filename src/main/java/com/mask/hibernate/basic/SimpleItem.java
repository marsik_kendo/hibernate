package com.mask.hibernate.basic;


import jakarta.persistence.*;

@Entity
@Table(name = "simple_items", schema = "hiber")
public class SimpleItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String name;

    @Override
    public String toString() {
        return "SimpleItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
