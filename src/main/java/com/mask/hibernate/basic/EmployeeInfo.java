package com.mask.hibernate.basic;

import jakarta.persistence.*;

@Entity
@Table(name = "emp_info")
public class EmployeeInfo {
    @Id
    @GeneratedValue
    @Column(name = "id")
    Long id;

    @Column
    String details;

    @OneToOne(mappedBy = "info")
    Employee employee;
}
